import {marked} from './../node_modules/marked/lib/marked.esm.js';

/*
before load page
*/

window.addEventListener('load', (event) => {
    document.querySelector('.load-bubble').classList.add('bubble-hide');
    document.querySelector('.container').style.opacity = 1;
    window.onload = typeText();
})

/*
    type subheader
 */
let typeObject = {
    typeText: ['Web Developer', 'Software Engineer', 'Cool Guy'],
    element: document.querySelector('.profile__subheading'),
    onText: "Web Developer",
    loopN: 0,
    isDelete: false,
    time: 200
}

const typeText = () => {

    typeObject.element.classList.remove('profile__subheading--border-animation');

    const i = typeObject.loopN % typeObject.typeText.length;
    const fullText = typeObject.typeText[i];

    if (typeObject.isDelete) {
        typeObject.onText = fullText.substring(0, typeObject.onText.length - 1);

    } else {
        typeObject.onText = fullText.substring(0, typeObject.onText.length + 1);
    }

    if (fullText === typeObject.onText && !typeObject.isDelete) {
        typeObject.isDelete = true;
        typeObject.time = 3000;
        typeObject.element.classList.add('profile__subheading--border-animation');

    } else if (typeObject.onText === '' && typeObject.isDelete) {
        typeObject.isDelete = false;
        typeObject.loopN++;
        typeObject.time = 200;
    }

    typeObject.element.innerHTML = typeObject.onText;
    setTimeout(function () {
        typeText();
    }, typeObject.time);

    if (typeObject.time === 3000) {
        typeObject.time = 50;

    }
}

/*
    age
 */

// const myBirthday = new Date('"1989/09/25"');
// const toDay = new Date();
// const month = toDay.getMonth() - myBirthday.getMonth();
// let myAge = toDay.getFullYear() - myBirthday.getFullYear();
// const ageElement = document.getElementById('age');
// if (month < 0) {
//     myAge--;
// }
// ageElement.innerHTML = myAge;


/*
    menu navigation
 */

function menu(el) {

    const onMenu = document.querySelector('.h-menu-active');
    onMenu.classList.remove('h-menu-active');

    el.classList.add('h-menu-active');

    const idOn = onMenu.getAttribute('data-menu');
    const containerRemoveAction = document.getElementById(idOn);
    containerRemoveAction.classList.remove('active');

    const idTo = el.getAttribute('data-menu');
    const containerAddAction = document.getElementById(idTo);

    containerAddAction.classList.add('active');
}


const menuButton = document.querySelectorAll('.nav__li');
menuButton.forEach(el => (
    el.addEventListener('click', () => {
        menu(el);
    })
))

//  Carousel

const navButtCaro = document.querySelectorAll('.nav-carousel');


function slideCaro(el) {

    const menu = document.querySelector('.nav-carousel--on');
    menu.classList.remove('nav-carousel--on');
    el.classList.add('nav-carousel--on');

    const getElementAttribute = el.getAttribute('data-testimonial');
    const allContainers = document.querySelectorAll('.container-move');


    for (let i = 0; i < allContainers.length; i++) {

        if (allContainers[i].getAttribute('data-testimonial-el') === getElementAttribute) {

            let activeElement = document.querySelector('.active--cont');

            if (allContainers[i].classList.contains('component--cont--left')) {


                activeElement.classList.add('component--cont--right');
                activeElement.classList.remove('active--cont');

                allContainers[i].classList.remove('component--cont--left');
                allContainers[i].classList.add('active--cont');


            } else if (allContainers[i].classList.contains('component--cont--right')) {

                activeElement.classList.add('component--cont--left');
                activeElement.classList.remove('active--cont');

                allContainers[i].classList.remove('component--cont--right');
                allContainers[i].classList.add('active--cont');


            }


        }
    }

}

navButtCaro.forEach(button => {
    button.addEventListener('click', () => slideCaro(button));
})


const rightBtn = document.getElementById('arrow-r');

rightBtn.addEventListener('click', () => {

    const getElementAttribute = document.querySelectorAll('.container-move');
    const bulletB = document.querySelectorAll('.nav-carousel')

    for (let i = 0; i < getElementAttribute.length; i++) {
        if (getElementAttribute[i].classList.contains('active--cont') && i !== getElementAttribute.length - 1) {
        // if (getElementAttribute[i].classList.contains('active--cont')) {
        //  TODO , fix this part
            getElementAttribute[i].classList.remove('active--cont');
            getElementAttribute[i].classList.add('component--cont--left');
            getElementAttribute[i + 1].classList.add('active--cont');
            getElementAttribute[i + 1].classList.remove('component--cont--left');
            getElementAttribute[i + 1].classList.remove('component--cont--right');

            bulletB[i].classList.remove('nav-carousel--on');
            bulletB[i + 1].classList.add('nav-carousel--on');
            break;
        }
    }

})

const leftBtn = document.getElementById('arrow-l');

leftBtn.addEventListener('click', () => {

    const getElementAttribute = document.querySelectorAll('.container-move');
    const bulletB = document.querySelectorAll('.nav-carousel')

    for (let i = getElementAttribute.length - 1; i > 0; i--) {
        if (getElementAttribute[i].classList.contains('active--cont') && i !== 0) {
            // if (getElementAttribute[i].classList.contains('active--cont')) {

            getElementAttribute[i].classList.add('component--cont--right');
            getElementAttribute[i].classList.remove('active--cont');
            getElementAttribute[i - 1].classList.add('active--cont');
            getElementAttribute[i - 1].classList.remove('component--cont--left');
            getElementAttribute[i - 1].classList.remove('component--cont--right');

            bulletB[i].classList.remove('nav-carousel--on');
            bulletB[i - 1].classList.add('nav-carousel--on');
            break;
        }
    }

})



const popEl = document.querySelector('.c-popup');

async function run(docs) {

    let file = await fetch(docs)
    let text = await file.text()
    document.getElementById('popup').innerHTML = marked.parse(text);
    popEl.classList.add('c-popup--pop');
    document.querySelector('.nav').classList.add('l-opacity');
}

const projects = document.querySelectorAll('[data-project]');

projects.forEach(el => {
    el.addEventListener('click', () => {
        const docs = "./../projects_info/" + el.getAttribute('data-project');
        run(docs)
    })
})

const closeProjectInfo = () => {
    popEl.classList.remove('c-popup--pop');
    document.querySelector('.nav').classList.remove('l-opacity');
}


document.addEventListener('keydown', el => {
    el.key === 'Escape' && popEl.classList.contains('c-popup--pop') && closeProjectInfo();
});

document.addEventListener('click', (el) => {
    !document.getElementById('popup').contains(el.target) && closeProjectInfo();
})